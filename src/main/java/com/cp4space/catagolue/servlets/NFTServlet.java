package com.cp4space.catagolue.servlets;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.census.CommonNames;
import com.cp4space.catagolue.patterns.GolObject;
import com.cp4space.payosha256.PayoshaUtils;
import com.cp4space.catagolue.utils.SvgUtils;
import com.cp4space.catagolue.servlets.HashsoupServlet;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;

public class NFTServlet extends ExampleServlet {

    String getTokenId(HttpServletRequest req) {

        String tokenId = null;
        String pathinfo = req.getPathInfo();

        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (tokenId == null)) {
                tokenId = pathParts[1];
            }
        }

        return tokenId;
    }

    String getNFTdata(HttpServletRequest req) {

        String tokenId = getTokenId(req);

        if (tokenId == null) { return null; }

        Map<String, String> namemap = new HashMap<String, String>();
        CommonNames.getNamemap("revnfts", namemap, false);

        if (namemap.containsKey(tokenId)) {
            return namemap.get(tokenId);
        } else {
            return null;
        }
    }

    @Override
    public String getTitle(HttpServletRequest req) {

        String nftdata = getNFTdata(req);

        if (nftdata == null) {
            return "Non-fungible tokens";
        } else {
            return nftdata.split("\\|")[0];
        }
    }

    @Override
    public void writeContent(PrintWriter writer, HttpServletRequest req) {

        String tokenId = getTokenId(req);
        String nftdata = getNFTdata(req);

        if (tokenId == null) {

            writer.println("<p>As of October 2021, Catagolue has added support to enable " +
                            "the discoverers of patterns in Conway's Game of Life to mint " +
                            "non-fungible tokens of their patterns and have them listed here.</p>");

            writer.println("<p>The NFTs are implemented by an <a href=\"https://etherscan.io/address/0xe4fa7749611334fc40f27f188a2f5e5b7016819b\">" +
                            "ERC721-compatible smart contract</a> on the Ethereum blockchain. " +
                            "The minting process is curated to ensure that only the genuine " +
                            "discoverer of a pattern can mint an NFT thereof.</p>");

            writer.println("<h3>Minted NFTs</h3>");

            writer.println("<p>NFTs that have been minted are <a href=\"https://opensea.io/collection/life-pattern\">listed on OpenSea</a>.</p>");

            // writer.println("<iframe src=\"https://opensea.io/assets/life-pattern?embed=true\" width=\"900\" height=\"750\"></iframe>");

            writer.println("<h3>Complete list</h3>");

            writer.println("<p>Each NFT is sequentially numbered by positive integer IDs in order of minting.</p><ol>");

            Map<String, String> namemap = new HashMap<String, String>();
            CommonNames.getNamemap("revnfts", namemap, false);

            for (int i = 1; i <= namemap.size(); i++) {
                String[] nftparts = namemap.get(String.valueOf(i)).split("\\|");
                String state = nftparts[1];

                if (state.charAt(0) == '0') {
                    if (nftparts[1].equals(nftparts[6])) {
                        state = "minted by " + nftparts[6];
                    } else {
                        state = "transferred/sold to " + nftparts[1];
                    }
                }

                writer.println("<li><b><a href=\"/nfts/" + i + "\">" + nftparts[0] + "</a></b> (" + state + ")</li>");
            }

            writer.println("</ol>");

        } else if (nftdata == null) {
            writer.println("<p>The token ID " + tokenId + " appears to be invalid.</p>");
        } else {

            String[] nftparts = nftdata.split("\\|");
            // 0: name
            // 1: state / current owner
            // 2: apgcode
            // 3: discoverer
            // 4: discovery date
            // 5: wiki page
            // 6: original owner

            writer.println("<table cellspacing=1><tr><td>");
            writer.println("<img src=\"/autogen/nfts/" + tokenId + ".gif\" />");
            writer.println("</td><td style=\"vertical-align:top\"><ul>");

            if (nftparts[1].equals("unminted")) {
                writer.println("<li>This NFT has <b>not yet been minted</b> on the Ethereum network.</li>");
            } else {
                writer.println("<li>This NFT has <b>been minted</b> on the Ethereum network and can be <a href=\"https://opensea.io/assets/0xe4fa7749611334fc40f27f188a2f5e5b7016819b/" + tokenId + "\">viewed on OpenSea</a>.</li>");
            }

            if (nftparts[2].length() > 0) {
                writer.println("<li>The underlying object is <a href=\"/object/" + nftparts[2] + "/b3s23\">on Catagolue</a>.</li>");
            }

            writer.println("<li>The pattern was discovered by " + nftparts[3] + " on " + nftparts[4] + ".</li>");

            if (nftparts[5].length() > 0) {
                writer.println("<li>The pattern has <a href=\"" + nftparts[5] + "\">a LifeWiki entry</a>.</li>");
            }

            writer.println("</ul></td></tr></table>");

            writer.println("<h3>JSON metadata</h3>");

            writer.println("<iframe src=\"/autogen/nfts/" + tokenId + ".json\" width=\"900\" height=\"300\"></iframe>");

            if (!(nftparts[1].equals("unminted"))) {
                writer.println("<h3>Ownership details</h3>");

                writer.println("<p><b>Original owner:</b> " + nftparts[6] + "</p>");
                writer.println("<p><b>Current owner:</b> "  + nftparts[1] + "</p>");

                writer.println("<nft-card contractAddress=\"0xe4fa7749611334fc40f27f188a2f5e5b7016819b\" tokenId=\"" + tokenId + "\"> </nft-card> <script src=\"https://unpkg.com/embeddable-nfts/dist/nft-card.min.js\"></script>");
            }
        }
    }
}
